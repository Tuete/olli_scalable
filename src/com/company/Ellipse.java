package com.company;

public class Ellipse implements Scalable
{
    private int breite, hoehe;

    public Ellipse(int breite, int hoehe) {
        this.breite = breite;
        this.hoehe = hoehe;
    }

    @Override
    public void Scale(int x, int y) {
        this.breite += x;
        this.hoehe += y;
    }
}
